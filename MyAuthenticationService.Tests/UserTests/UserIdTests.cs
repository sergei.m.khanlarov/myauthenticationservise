﻿using System;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class UserIdTests
{
    private readonly string _testId = "UserId_B48C4FACEE5E4A2985C49BBD573491C6";
    
    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<NullReferenceException>(() => UserId.Parse(null));
        Assert.Throws<ArgumentException>(() => UserId.Parse("wrongUserId"));
        Assert.Throws<ArgumentException>(() => UserId.Parse(" "));
        Assert.Throws<ArgumentException>(() => UserId.Parse("wrong_UserId"));
        Assert.Throws<ArgumentException>(() => UserId.Parse("wrongUserId_B48C4FACEE5E4A2985C49BBD573491C6"));
        Assert.Throws<ArgumentException>(() => UserId.Parse("UserId_wrongGuid"));
    }
    
    [Fact]
    public void CreationTest_20()
    {
        Assert.True(UserId.Parse(_testId).ToString().Equals(_testId));
    }
    
    [Fact]
    public void CreationTest_30()
    {
        var id = UserId.New();
        Assert.True(UserId.Parse(id.ToString()).ToString().Equals(id.ToString()));
    }
}