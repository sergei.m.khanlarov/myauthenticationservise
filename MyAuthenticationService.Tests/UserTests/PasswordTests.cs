﻿using System;
using System.Security.Cryptography;
using System.Text;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class PasswordTests
{
    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<ArgumentNullException>(() => Password.Parse(null));
        Assert.Throws<ArgumentNullException>(() => Password.Parse(""));
        Assert.Throws<ArgumentNullException>(() => Password.Parse(" "));
    }
    
    [Fact]
    public void CreationTest_20()
    {
        Assert.Throws<ArgumentException>(() => Password.Parse("str"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string|"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string{"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string*"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string}"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string+"));
        Assert.Throws<ArgumentException>(() => Password.Parse("string4"));
        Assert.Throws<ArgumentException>(() => Password.Parse("String"));
        Assert.Throws<ArgumentException>(() => Password.Parse("STRING"));
    }
    
    [Fact]
    public void CreationTest_30()
    {
        var password = "String1";
        
        Assert.True(Password.ParseNoHashing(password).ToString().Equals(password));
    }
    
    [Fact]
    public void CreationTest_31()
    {
        var password = "String1";
        var hashedPassword = HashPassword("String1");
        
        Assert.True(Password.Parse(password).ToString().Equals(hashedPassword));
    }
    
    private static string HashPassword(string password)
    {
        MD5 md5 = MD5.Create();

        byte[] b = Encoding.ASCII.GetBytes(password);
        byte[] hash = md5.ComputeHash(b);

        var sb = new StringBuilder();

        foreach (var a in hash)
            sb.Append(a.ToString("X2"));

        return Convert.ToString(sb);
    }
}