﻿using System;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class NameTests
{
    private const string first = "Joe";
    private const string middle = "Bennet";
    private const string last = "Smith";

    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<ArgumentException>(() => Name.Parse(first + "!", last));
        Assert.Throws<ArgumentException>(() => Name.Parse(first + "#", last));
        Assert.Throws<ArgumentException>(() => Name.Parse(first + "@", last));
        Assert.Throws<ArgumentException>(() => Name.Parse(first + "?", last));
    }


    [Fact]
    public void CreationTest_12()
    {
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last + "!"));
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last + "#"));
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last + "@"));
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last + "?"));
    }

    [Fact]
    public void CreationTest_13()
    {
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last, middle + "!"));
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last, middle + "#"));
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last, middle + "@"));
        Assert.Throws<ArgumentException>(() => Name.Parse(first, last, middle + "?"));
    }


    [Fact]
    public void CreationTest_20()
    {
        Assert.Throws<ArgumentNullException>(() => Name.Parse(null, last, middle));
        Assert.Throws<ArgumentNullException>(() => Name.Parse("", last, middle));
        Assert.Throws<ArgumentNullException>(() => Name.Parse(" ", last, middle));
    }
    
    [Fact]
    public void CreationTest_21()
    {
        Assert.Throws<ArgumentNullException>(() => Name.Parse(first, null, middle));
        Assert.Throws<ArgumentNullException>(() => Name.Parse(first, "", middle));
        Assert.Throws<ArgumentNullException>(() => Name.Parse(first, " ", middle));
    }

    [Fact]
    public void CreationTest_30()
    {
        var name = $"{last} {first} {middle}";;

        Assert.True(Name.Parse(first, last, middle).ToString().Equals(name));
    }
    
    [Fact]
    public void CreationTest_31()
    {
        var name = $"{last} {first}";;

        Assert.True(Name.Parse(first, last).ToString().Equals(name));
    }
}