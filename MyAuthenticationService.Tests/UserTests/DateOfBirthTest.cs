﻿using System;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class DateOfBirthTest
{
    [Fact]
    public void CreationTest_1()
    {
        Assert.Throws<ArgumentException>(() => DateOfBirth.Parse(DateOnly.Parse(DateTime.UtcNow.ToString("d"))));
    }
    
    [Fact]
    public void CreationTest_2()
    {
        var date = DateTime.UtcNow;

        Assert.Throws<ArgumentException>(() => DateOfBirth.Parse(new DateOnly(year: date.Year - 18,
                                                                              month: date.Month,
                                                                              day: date.Day + 2)));
    }

    [Fact]
    public void CreationTest_3()
    {
        var date = new DateOnly(year: 1991, month: 1, day: 7);

        var Dob = DateOfBirth.Parse(date);
        
        Assert.True(date.ToLongDateString().Equals(Dob.ToString()));
    }
}