﻿using System;

using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.Enums;
using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class UserTestsClass
{
    private readonly UserId _id = UserId.New();
    private readonly Login _login = Login.Parse("UserLogin");
    private readonly Password _password = Password.Parse("String1");
    private readonly DateOfBirth _dateOfBirth = DateOfBirth.Parse(new DateOnly(1991, 1, 1));
    private readonly EmaiL _emaiL = EmaiL.Parse("addres@domain.ru");
    private readonly Name _name = Name.Parse("first", "last", "middle");
    private readonly Role _role = Role.User;

    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<ArgumentNullException>(() => new User(id: null,
                                                            login: _login,
                                                            password: _password,
                                                            dateOfBirth: _dateOfBirth,
                                                            email: _emaiL,
                                                            name: _name,
                                                            role: _role));

        Assert.Throws<ArgumentNullException>(() => new User(id: _id,
                                                            login: null,
                                                            password: _password,
                                                            dateOfBirth: _dateOfBirth,
                                                            email: _emaiL,
                                                            name: _name,
                                                            role: _role));

        Assert.Throws<ArgumentNullException>(() => new User(id: _id,
                                                            login: _login,
                                                            password: null,
                                                            dateOfBirth: _dateOfBirth,
                                                            email: _emaiL,
                                                            name: _name,
                                                            role: _role));

        Assert.Throws<ArgumentNullException>(() => new User(id: _id,
                                                            login: _login,
                                                            password: _password,
                                                            dateOfBirth: null,
                                                            email: _emaiL,
                                                            name: _name,
                                                            role: _role));

        Assert.Throws<ArgumentNullException>(() => new User(id: _id,
                                                            login: _login,
                                                            password: _password,
                                                            dateOfBirth: _dateOfBirth,
                                                            email: null,
                                                            name: _name,
                                                            role: _role));

        Assert.Throws<ArgumentNullException>(() => new User(id: _id,
                                                            login: _login,
                                                            password: _password,
                                                            dateOfBirth: _dateOfBirth,
                                                            email: _emaiL,
                                                            name: null,
                                                            role: _role));
    }

    [Fact]
    public void CreationTest_20()
    {
        Assert.True(CreateTestUser().Id.ToString().Equals(_id.ToString()));
        Assert.True(CreateTestUser().Login.ToString().Equals(_login.ToString()));
        Assert.True(CreateTestUser().Password.ToString().Equals(_password.ToString()));
        Assert.True(CreateTestUser().DateOfBirth.ToString().Equals(_dateOfBirth.ToString()));
        Assert.True(CreateTestUser().EmaiL.ToString().Equals(_emaiL.ToString()));
        Assert.True(CreateTestUser().Name.ToString().Equals(_name.ToString()));
        Assert.True(CreateTestUser().Role.ToString().Equals(_role.ToString()));
    }

    [Fact]
    public void UpdateTest_IsDeleted()
    {
        var user = CreateTestUser();
        Assert.True(user.IsDeleted == false);
        user.Delete();
        Assert.True(user.IsDeleted);
    }
    
    [Fact]
    public void UpdateTest_Password_1()
    {
        var user = CreateTestUser();
        
        Assert.True(user.Password.ToString().Equals(_password.ToString()));
        
        var newPassword = Password.Parse("String22");
        
        user.SetNewPassword(newPassword);
        
        Assert.False(user.Password.ToString().Equals(_password.ToString()));
        Assert.True(user.Password.ToString().Equals(newPassword.ToString()));
    }
    
    [Fact]
    public void UpdateTest_Password_2()
    {
        var user = CreateTestUser();
        Assert.True(user.Password.ToString().Equals(_password.ToString()));
        Assert.Throws<ArgumentNullException>(() =>  user.SetNewPassword(null));
    }
    
    [Fact]
    public void UpdateTest_Password_3()
    {
        var user = CreateTestUser();
        Assert.True(user.Password.ToString().Equals(_password.ToString()));
        Assert.Throws<ArgumentException>(() =>  user.SetNewPassword(_password));
    }
    
    [Fact]
    public void UpdateTest_Email_1()
    {
        var user = CreateTestUser();
        Assert.True(user.EmaiL.ToString().Equals(_emaiL.ToString()));
        var newEmail = EmaiL.Parse("newAddress@AnotherDomain.com");
        user.UpdateEmail(newEmail);
        Assert.False(user.EmaiL.ToString().Equals(_emaiL.ToString()));
        Assert.True(user.EmaiL.ToString().Equals(newEmail.ToString()));
    }
    
    [Fact]
    public void UpdateTest_Email_2()
    {
        var user = CreateTestUser();
        Assert.True(user.EmaiL.ToString().Equals(_emaiL.ToString()));
        Assert.Throws<ArgumentNullException>(() =>  user.UpdateEmail(null));
    }
    
    [Fact]
    public void UpdateTest_Email_3()
    {
        var user = CreateTestUser();
        Assert.True(user.EmaiL.ToString().Equals(_emaiL.ToString()));
        Assert.Throws<ArgumentException>(() =>  user.UpdateEmail(_emaiL));
    }
    
    [Fact]
    public void UpdateTest_1()
    {
        var user = CreateTestUser();
        Assert.True(user.Name.ToString().Equals(_name.ToString()));
        Assert.True(user.DateOfBirth.ToString().Equals(_dateOfBirth.ToString()));
        
        var dateOfBirth = DateOfBirth.Parse(new DateOnly(2001, 1, 1));
        var name = Name.Parse("firstnew", "lastnew", "middlenew");
        
        user.Update(dateOfBirth, name);
        
        Assert.False(user.Name.ToString().Equals(_name.ToString()));
        Assert.False(user.DateOfBirth.ToString().Equals(_dateOfBirth.ToString()));
        
        Assert.True(user.Name.ToString().Equals(name.ToString()));
        Assert.True(user.DateOfBirth.ToString().Equals(dateOfBirth.ToString()));
    }
    
    [Fact]
    public void UpdateTest_2()
    {
        var user = CreateTestUser();
        Assert.True(user.Name.ToString().Equals(_name.ToString()));
        Assert.True(user.DateOfBirth.ToString().Equals(_dateOfBirth.ToString()));
        
        var dateOfBirth = DateOfBirth.Parse(new DateOnly(2001, 1, 1));
        var name = Name.Parse("firstnew", "lastnew", "middlenew");
        
        Assert.Throws<ArgumentNullException>(() =>  user.Update(null, name));
        Assert.Throws<ArgumentNullException>(() =>  user.Update(dateOfBirth, null));
    }
    
    [Fact]
    public void UpdateTest_Role_1()
    {
        var user = CreateTestUser(Role.User);
        
        Assert.True(user.Role.ToString().Equals(_role.ToString()));

        Assert.Throws<ArgumentException>(() =>  user.ChangeRole(Role.User));
    }
    
    [Fact]
    public void UpdateTest_Role_2()
    {
        var user = CreateTestUser(Role.User);
        
        Assert.True(user.Role.ToString().Equals(_role.ToString()));

        user.ChangeRole(Role.Admin);

        Assert.True(user.Role.ToString().Equals(Role.Admin.ToString()));
    }

    private User CreateTestUser() => new User(id: _id,
                                              login: _login,
                                              password: _password,
                                              dateOfBirth: _dateOfBirth,
                                              email: _emaiL,
                                              name: _name,
                                              role: _role);
    private User CreateTestUser(Role role) => new User(id: _id,
                                              login: _login,
                                              password: _password,
                                              dateOfBirth: _dateOfBirth,
                                              email: _emaiL,
                                              name: _name,
                                              role: role);
}