﻿using System;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class EmaiLTests
{
    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<ArgumentException>(() => EmaiL.Parse("strruru"));
        Assert.Throws<ArgumentNullException>(() => EmaiL.Parse(""));
        Assert.Throws<ArgumentNullException>(() => EmaiL.Parse(" "));
        Assert.Throws<ArgumentNullException>(() => EmaiL.Parse(null));
    }

    [Fact]
    public void CreationTest_20()
    {
        var email = "name@domain.ru";

        Assert.True(EmaiL.Parse(email).ToString().Equals(email));
    }
}