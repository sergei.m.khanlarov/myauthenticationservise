﻿using System;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.UserTests;

public class LoginTests
{
    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<ArgumentNullException>(() => Login.Parse(null));
        Assert.Throws<ArgumentNullException>(() => Login.Parse(""));
        Assert.Throws<ArgumentNullException>(() => Login.Parse(" "));
    }

    [Fact]
    public void CreationTest_20()
    {
        var login = "login";

        Assert.True(Login.Parse(login).ToString().Equals(login));
    }
    
    [Fact]
    public void CreationTest_4()
    {
        var login = "login@";

        Assert.True(Login.Parse(login).ToString().Equals(login));
    }
    
    [Fact]
    public void CreationTest_5()
    {
        var login = "login112";

        Assert.True(Login.Parse(login).ToString().Equals(login));
    }
}