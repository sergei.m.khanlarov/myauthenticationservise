﻿using System;

using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.RecordChangedTests;

public class RecordChangedIdTests
{
    private readonly string _testId = "RecordChangedId_B48C4FACEE5E4A2985C49BBD573491C6";
    
    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<NullReferenceException>(() => RecordChangedId.Parse(null));
        Assert.Throws<ArgumentException>(() => RecordChangedId.Parse("wrongUserId"));
        Assert.Throws<ArgumentException>(() => RecordChangedId.Parse(" "));
        Assert.Throws<ArgumentException>(() => RecordChangedId.Parse("wrong_UserId"));
        Assert.Throws<ArgumentException>(() => RecordChangedId.Parse("wrongRecordChangedId_B48C4FACEE5E4A2985C49BBD573491C6"));
        Assert.Throws<ArgumentException>(() => RecordChangedId.Parse("RecordChangedId_wrongGuid"));
    }
    
    [Fact]
    public void CreationTest_20()
    {
        Assert.True(RecordChangedId.Parse(_testId).ToString().Equals(_testId));
    }
    
    [Fact]
    public void CreationTest_30()
    {
        var id = RecordChangedId.New();
        Assert.True(RecordChangedId.Parse(id.ToString()).ToString().Equals(id.ToString()));
    }
}