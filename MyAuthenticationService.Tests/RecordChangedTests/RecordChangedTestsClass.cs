﻿using System;

using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.ValueObjects;

using Xunit;

namespace MyAuthenticationService.Tests.RecordChangedTests;

public class RecordChangedTestsClass
{
    private readonly RecordChangedId _id = RecordChangedId.New();
    private readonly string _userId = UserId.New().ToString();
    private readonly string _userIdChangedBy = UserId.New().ToString();
    private readonly string _property = "propertyName";
    private readonly string _oldValue = "oldValue";
    private readonly string _newValue = "propertyName";

    [Fact]
    public void CreationTest_10()
    {
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: null,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId:  null,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId:  "",
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId:  " ",
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: null,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: "",
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: " ",
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: null,
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: "",
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: " ",
                                                                     oldValue: _oldValue,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: null,
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: "",
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: " ",
                                                                     newValue: _newValue));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: null));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: ""));
        Assert.Throws<ArgumentNullException>(() => new RecordChanged(id: _id,
                                                                     userId: _userId,
                                                                     changedByUserId: _userIdChangedBy,
                                                                     property: _property,
                                                                     oldValue: _oldValue,
                                                                     newValue: " "));
    }
}