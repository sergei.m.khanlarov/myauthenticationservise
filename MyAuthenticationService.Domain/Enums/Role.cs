﻿namespace MyAuthenticationService.Domain.Enums;

public enum Role
{
    /// <summary>
    /// Админ
    /// </summary>
    Admin,
    
    /// <summary>
    /// Пользователь
    /// </summary>
    User
}