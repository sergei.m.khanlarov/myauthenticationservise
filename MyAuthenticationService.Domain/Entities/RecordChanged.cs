﻿using MyAuthenticationService.Domain.Base;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Domain.Entities;

public class RecordChanged : Entity<RecordChangedId>
{
    /// <summary>
    /// чью запись меняли
    /// </summary>
    public string UserId { get; private set; }
    
    /// <summary>
    /// когда
    /// </summary>
    public DateTime ChangedAt { get; private set; }
    
    /// <summary>
    /// кто
    /// </summary>
    public string ChangedByUserId { get; private set; }
    
    /// <summary>
    /// Какое из свойств
    /// </summary>
    public string Property { get; private set; }

    /// <summary>
    /// Старое значение
    /// </summary>
    public string OldValue { get; private set;  }
    
    /// <summary>
    /// Новое значение
    /// </summary>
    public string NewValue { get; private set; }
    
    private RecordChanged(RecordChangedId id) : base(id)
    {
    }

    public RecordChanged(RecordChangedId id, string userId,
                         string changedByUserId,
                         string property,
                         string oldValue,
                         string newValue) : base(id)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));
        if (string.IsNullOrWhiteSpace(changedByUserId))
            throw new ArgumentNullException(nameof(changedByUserId));
        if (string.IsNullOrWhiteSpace(property))
            throw new ArgumentNullException(nameof(property));
        if (string.IsNullOrWhiteSpace(oldValue))
            throw new ArgumentNullException(nameof(oldValue));
        if (string.IsNullOrWhiteSpace(newValue))
            throw new ArgumentNullException(nameof(newValue));
        
        UserId = userId;
        ChangedAt = DateTime.UtcNow;
        ChangedByUserId = changedByUserId;
        Property = property;
        OldValue = oldValue;
        NewValue = newValue;
    }
}