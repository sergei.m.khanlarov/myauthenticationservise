﻿using MyAuthenticationService.Domain.Base;
using MyAuthenticationService.Domain.Enums;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Domain.Entities;

public class User : Entity<UserId>
{
    /// <summary>
    /// login
    /// </summary>
    public Login  Login { get; private set; }
    
    /// <summary>
    /// password
    /// </summary>
    public Password Password { get; private set; }
    
    /// <summary>
    /// Date of dirth
    /// </summary>
    public DateOfBirth DateOfBirth { get; private set; }
    
    /// <summary>
    /// Email
    /// </summary>
    public EmaiL EmaiL { get; private set; }
    
    /// <summary>
    /// Name
    /// </summary>
    public Name Name { get; private set; }
    
    /// <summary>
    /// Роль
    /// </summary>
    public Role Role { get; private set; }
    
    /// <summary>
    /// Is deleted
    /// </summary>
    public bool IsDeleted { get; private set; }

    private User(UserId id) : base(id)
    {
    }

    public User(UserId id, Login login, Password password,
                DateOfBirth dateOfBirth,
                EmaiL email,
                Name name,
                Role role) : base(id)
    {
        Login = login ?? throw new ArgumentNullException(nameof(login));
        Password = password ?? throw new ArgumentNullException(nameof(password));
        DateOfBirth = dateOfBirth ?? throw new ArgumentNullException(nameof(dateOfBirth));
        Name = name ?? throw new ArgumentNullException(nameof(name));
        EmaiL = email ?? throw new ArgumentNullException(nameof(email));
        Role = role;

        IsDeleted = false;
    }

    /// <summary>
    /// Удаление пользователя (мягкое)
    /// </summary>
    public void Delete() => IsDeleted = true;

    /// <summary>
    /// Обновляет данные о пользователе
    /// </summary>
    /// <param name="dateOfBirth">день рождения</param>
    /// <param name="name">имя</param>
    /// <exception cref="ArgumentNullException"></exception>
    public void Update(DateOfBirth dateOfBirth, Name name)
    {
        if (dateOfBirth is null)
            throw new ArgumentNullException(nameof(dateOfBirth));

        if (!DateOfBirth.Equals(dateOfBirth))
            DateOfBirth = dateOfBirth;

        if (name is null)
            throw new ArgumentNullException(nameof(name));

        if (!Name.Equals(name))
            Name = name;
    }

    /// <summary>
    /// Меняет роль
    /// </summary>
    /// <param name="newRole">новая роль</param>
    public void ChangeRole(Role newRole)
    {
        if (Role == newRole)
            throw new ArgumentException("You are trying to set the same role");
            
        Role = newRole;
    }

    /// <summary>
    /// Обновление почты
    /// </summary>
    /// <param name="newEmail"></param>
    public void UpdateEmail(EmaiL newEmail)
    {
        if (newEmail is null)
            throw new ArgumentNullException(nameof(newEmail));

        if (EmaiL.Equals(newEmail))
            throw new ArgumentException("New and old emails are the same");
        
        EmaiL = newEmail;
    }

    /// <summary>
    /// Обновление пароля
    /// </summary>
    /// <param name="password"></param>
    public void SetNewPassword(Password password)
    {
        if (password is null)
            throw new ArgumentNullException(nameof(password));

        if (Password.Equals(password))
            throw new ArgumentException("New and old passwords are the same");
        
        Password = password;
    }
}