﻿namespace MyAuthenticationService.Domain.Base;

/// <summary>
/// Базовый класс Id
/// </summary>
public abstract class BaseId : ValueObject
{
    protected readonly string _id;

    protected BaseId(string id)
    {
        _id = id;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return _id;
    }

    public override string ToString()
    {
        return _id;
    }
}