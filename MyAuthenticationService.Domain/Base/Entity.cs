﻿namespace MyAuthenticationService.Domain.Base;

/// <summary>
/// Базовый класс сущности
/// </summary>
public abstract class Entity<TId> where TId : BaseId
{
    /// <summary>
    /// Id сущности
    /// </summary>
    public TId Id { get; }
    
    protected Entity(TId id) => Id = id ?? throw new ArgumentNullException(nameof(id));
}