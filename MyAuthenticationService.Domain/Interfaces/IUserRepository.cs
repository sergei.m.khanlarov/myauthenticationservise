﻿using System.Linq.Expressions;

using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Domain.Interfaces;

public interface IUserRepository
{
    Task<User> GetByIdAsync(UserId id);

    Task<ICollection<User?>> GetAllAsync();

    IQueryable<User?> GetAll();

    Task<ICollection<User?>> GetUsers(Expression<Func<User?, bool>> filter = default);

    Task AddAsync(User? user);
    
    Task UpdateAsync(User? user);

    Task<User?> GetByLoginAsync(Login login);
}
