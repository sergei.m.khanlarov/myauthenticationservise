﻿using System.Linq.Expressions;

using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Domain.Interfaces;

public interface IRecordChangedRepository
{
    Task<RecordChanged> GetByIdAsync(RecordChangedId id);

    Task<ICollection<RecordChanged?>> GetAllAsync();

    IQueryable<RecordChanged> GetAll();

    Task<ICollection<RecordChanged?>> GetRecords(Expression<Func<RecordChanged?, bool>> filter = default);

    Task AddAsync(RecordChanged? record);
}
