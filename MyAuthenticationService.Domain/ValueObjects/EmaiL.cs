﻿using System.ComponentModel.DataAnnotations;

using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Domain.ValueObjects;

public class EmaiL : ValueObject
{
    /// <summary>
    /// email
    /// </summary>
    private readonly string _email;

    //ef core
    private EmaiL()
    { }
    
    private EmaiL(string email) => _email = email;

    /// <summary>
    /// parse email
    /// </summary>
    /// <param name="email">user email</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"> if email is null</exception>
    /// <exception cref="ArgumentException">if email is in incorrect format</exception>
    public static EmaiL Parse(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
            throw new ArgumentNullException("email is empty");

        if (!new EmailAddressAttribute().IsValid(email))
            throw new ArgumentException("Email is in incorrect format");
        
        return new EmaiL(email);
    }

    public override string ToString() => _email;

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return _email;
    }
}