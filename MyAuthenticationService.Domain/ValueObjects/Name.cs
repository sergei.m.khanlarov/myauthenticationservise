﻿using System.Text.RegularExpressions;

using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Domain.ValueObjects;

/// <summary>
/// Name Value Object
/// </summary>
public class Name : ValueObject
{
    /// <summary>
    /// first name
    /// </summary>
    public string First { get; private set; }
    
    /// <summary>
    /// middle name
    /// </summary>
    public string? Middle { get; private set; }
    
    /// <summary>
    /// last name
    /// </summary>
    public string Last { get; private set; }

    //ef core
    private Name()
    { }
    
    private Name(string first, string last, string? middle = null)
    {
        First = first;
        Last = last;
        Middle = middle;
    }

    /// <summary>
    /// Создает класс имя
    /// </summary>
    /// <param name="first">имя</param>
    /// <param name="last">фамилия</param>
    /// <param name="middle">отчество</param>
    /// <returns></returns>
    /// <exception cref="ArgumentException">пробрасывается если неверный формат</exception>
    /// <exception cref="ArgumentNullException">пробрасывается если пустое значение у имени или фамилии</exception>
    public static Name Parse(string first, string last, string? middle = null)
    {
        Regex r = new Regex(@"[\d!#@?]");

        if (r.Matches(first).Count > 0)
            throw new ArgumentException("first name is in incorrect format");
        
        if (r.Matches(last).Count > 0)
            throw new ArgumentException("first name is in incorrect format");
        
        if (middle is not null && r.Matches(middle).Count > 0)
            throw new ArgumentException("middle name is in incorrect format");

        if (string.IsNullOrWhiteSpace(first))
            throw new ArgumentNullException(nameof(first));
        
        if (string.IsNullOrWhiteSpace(last))
            throw new ArgumentNullException(nameof(last));
        
        return new Name(first, last, middle);
    }

    public override string ToString()
    {
        if (string.IsNullOrWhiteSpace(Middle))
            return $"{Last} {First}";

        return $"{Last} {First} {Middle}";
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return First;
        yield return Last;
        yield return Middle;
    }
}