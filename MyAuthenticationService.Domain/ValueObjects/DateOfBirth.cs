﻿using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Domain.ValueObjects;

/// <summary>
/// DateOfBirth value object
/// </summary>
public class DateOfBirth : ValueObject
{
    /// <summary>
    /// DateOfBirth
    /// </summary>
    private readonly DateOnly _dateOfBirth;

    /// <summary>
    /// years to validation
    /// </summary>
    private static int years = 18;

    //ef core
    private DateOfBirth()
    { }
    
    private DateOfBirth(DateOnly dateOfBirth) => _dateOfBirth = dateOfBirth;

    /// <summary>
    /// Parse date of birth if the user 18+ years old
    /// </summary>
    /// <param name="dateOfBirth">date of birth</param>
    /// <returns></returns>
    /// <exception cref="ArgumentException">if not 18+</exception>
    public static DateOfBirth Parse(DateOnly dateOfBirth)
    {
        var date = new DateTime(dateOfBirth.Year, dateOfBirth.Month, dateOfBirth.Day).AddYears(years);

        var today = DateTime.UtcNow;

        if (date > today)
            throw new ArgumentException("you are not 18+ years old");

        return new DateOfBirth(dateOfBirth);
    }

    public override string ToString() => _dateOfBirth.ToLongDateString();

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return _dateOfBirth;
    }
}