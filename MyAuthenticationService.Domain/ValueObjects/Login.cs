﻿using System.ComponentModel.DataAnnotations;

using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Domain.ValueObjects;

/// <summary>
/// Login Value Object
/// </summary>
public class Login : ValueObject
{
    /// <summary>
    /// Логин
    /// </summary>
    private readonly string _login;

    //ef core
    private Login()
    { }
    
    private Login(string login) => _login = login;

    /// <summary>
    /// Парсинг логина, создание ллгина на основе значения
    /// </summary>
    /// <param name="login">лошин</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">пробрасывается если значение логина пустое</exception>
    public static Login Parse(string login)
    {
        if (string.IsNullOrWhiteSpace(login))
            throw new ArgumentNullException(nameof(login));

        return new Login(login);
    }

    public override string ToString() => _login;

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return _login;
    }
}