﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Domain.ValueObjects;

/// <summary>
/// Passwword value object
/// </summary>
public class Password : ValueObject
{
    /// <summary>
    /// пароль
    /// </summary>
    private readonly string _password;

    //ef core
    private Password()
    { }
    
    private Password(string password) => _password = password;

    /// <summary>
    /// для создания пароля на базе строки
    /// </summary>
    /// <param name="password">пароль</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">Если знаечние пароля пустое</exception>
    public static Password Parse(string password)
    {
        if (string.IsNullOrWhiteSpace(password))
            throw new ArgumentNullException(nameof(password));

        CheckPassword(password);
        
        var hashPassword = HashPassword(password);

        return new Password(hashPassword);
    }
    
    /// <summary>
    /// для создания пароля на базе строки
    /// </summary>
    /// <param name="password">пароль</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">Если знаечние пароля пустое</exception>
    public static Password ParseForLogin(string password)
    {
        if (string.IsNullOrWhiteSpace(password))
            throw new ArgumentNullException(nameof(password));

        var hashPassword = HashPassword(password);

        return new Password(hashPassword);
    }
    
    /// <summary>
    /// для создания пароля на базе строки (только для получение пароля из БД)
    /// </summary>
    /// <param name="password">пароль</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static Password ParseNoHashing(string password)
    {
        if (string.IsNullOrWhiteSpace(password))
            throw new ArgumentNullException(nameof(password));

        return new Password(password);
    }

    private static void CheckPassword(string password)
    {
        if (password.Length < 6 ||
            new Regex(@"[*&{}|+,]+").IsMatch(password) || 
            !new Regex(@"[A-Z]+").IsMatch(password) ||
            !new Regex(@"[a-z]+").IsMatch(password) || 
            !new Regex(@"[0-9]+").IsMatch(password))
            throw new ArgumentException($"Incorrect password" +
                                        $"must contain at least 6 characters," +
                                        "must not be characters from the set: * & {} | +," +
                                        "must contain capital and non-capital letters," +
                                        "must contain numbers");
    }

    private static string HashPassword(string password)
    {
        MD5 md5 = MD5.Create();

        byte[] b = Encoding.ASCII.GetBytes(password);
        byte[] hash = md5.ComputeHash(b);

        var sb = new StringBuilder();

        foreach (var a in hash)
            sb.Append(a.ToString("X2"));

        return Convert.ToString(sb);
    }
    
    public override string ToString() => _password;

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return _password;
    }
}