﻿using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Domain.ValueObjects;

public class RecordChangedId : BaseId
{
    private RecordChangedId(string id) : base(id)
    {
    }
    
    /// <summary>
    /// Принимает значение и рперевлдит его в идентификатор
    /// </summary>
    /// <param name="value">значение</param>
    /// <returns></returns>
    /// <exception cref="ArgumentException">если значение в неверном формате</exception>
    public static RecordChangedId Parse(string value)
    {
        var idTmp = value.Split('_');

        if (idTmp.Length != 2 || !idTmp[0].Equals(nameof(RecordChangedId)) || !Guid.TryParse(idTmp[1], out _))
            throw new ArgumentException($"value {value} is invalid guidance id");

        return new RecordChangedId(value);
    }

    /// <summary>
    /// Создает новый идентификатор
    /// </summary>
    /// <returns></returns>
    public static RecordChangedId New() => new($"{nameof(RecordChangedId)}_{Guid.NewGuid():N}");

    public override string ToString() => _id;
}