﻿using System.Security.Claims;

using Microsoft.AspNetCore.Http;

namespace MyAuthenticationService.Application.Services;

public class AuthenticationServiceCustom
{
    private readonly IHttpContextAccessor _context;
    
    public AuthenticationServiceCustom(IHttpContextAccessor context) => _context = context;

    public bool IsAllowed(string userId)
    {
        if (_context.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value == "Admin")
            return true;

        if (_context.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "id")?.Value == userId)
            return true;

        return false;
    }
}