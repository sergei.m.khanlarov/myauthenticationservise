﻿using System.Diagnostics;

using MediatR;

using Serilog;

namespace MyAuthenticationService.Application.Behaviours;

public class LoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> 
        where TRequest : IRequest<TResponse>
{
        private readonly ILogger _logger;
        private readonly Stopwatch _timer;

        public LoggingBehaviour(ILogger logger)
        {
                _logger = logger;
                _timer = new Stopwatch();
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
                var requestName = typeof(TRequest).Name;
        
                _logger.Information("Handling {Name} start", requestName);
                _logger.Information("Request {@Request}", request);

                TResponse response;
                try
                {
                        _timer.Start();
                        response = await next();
                        _logger.Information("Response {@Response}", response);
                }
                catch (Exception ex)
                {
                        _logger.Error(ex, "Request: Unhandled Exception for Request {Name}", requestName);

                        throw;
                }
                finally
                {
                        _timer.Stop();
                        var elapsedMilliseconds = _timer.ElapsedMilliseconds;
            
                        _logger.Warning("Request handled in: {ElapsedMilliseconds} milliseconds", elapsedMilliseconds);

                        _logger.Information("Handled {Name} finish", requestName);
                }

                return response;
        }
}