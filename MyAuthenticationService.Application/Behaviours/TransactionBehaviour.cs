﻿using MediatR;

using Microsoft.EntityFrameworkCore;

using MyAuthenticationService.Application.Interfaces;

using Serilog;

namespace MyAuthenticationService.Application.Behaviours;

public class TransactionBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : IRequest<TResponse>
{
    private readonly ILogger _logger;
    private readonly IDbTransactionServices _transaction;

    public TransactionBehaviour(IDbTransactionServices dbContext, ILogger logger)
    {
        _transaction = dbContext ?? throw new ArgumentException(nameof(dbContext));
        _logger = logger ?? throw new ArgumentException(nameof(logger));
    }

    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        var requestName = typeof(TRequest).Name;
        TResponse response = default;

        try
        {
            var strategy = _transaction.CreateExecutionStrategy();
            await strategy.ExecuteAsync(async () =>
            {
                _logger.Information("Begin transaction {Name}", requestName);

                await _transaction.BeginTransactionAsync();

                response = await next();

                await _transaction.CommitTransactionAsync();

                _logger.Information("Committed transaction {Name}", requestName);
            });

            return response;
        }
        catch (Exception)
        {
            _logger.Information("Rollback transaction executed {Name}", requestName);

            await _transaction.RollbackTransaction();
            throw;
        }
    }
}