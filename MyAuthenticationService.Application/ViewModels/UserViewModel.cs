﻿namespace MyAuthenticationService.Application.ViewModels;

public class UserViewModel
{
    /// <summary>
    /// login
    /// </summary>
    public string  Login { get; init; }

    /// <summary>
    /// Date of dirth
    /// </summary>
    public string DateOfBirth { get; init; }
    
    /// <summary>
    /// Email
    /// </summary>
    public string EMaiL { get; init; }
    
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; init; }

    /// <summary>
    /// Роль
    /// </summary>
    public string Role { get; init; }
}