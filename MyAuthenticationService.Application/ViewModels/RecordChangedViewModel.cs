﻿namespace MyAuthenticationService.Application.ViewModels;

public class RecordChangedViewModel
{
    /// <summary>
    /// чью запись меняли
    /// </summary>
    public string UserId { get; init; }
    
    /// <summary>
    /// когда
    /// </summary>
    public DateTime ChangedAt { get; init; }
    
    /// <summary>
    /// кто
    /// </summary>
    public string ChangedByUserId { get; init; }
    
    /// <summary>
    /// Какое из свойств
    /// </summary>
    public string Property { get; init; }

    /// <summary>
    /// Старое значение
    /// </summary>
    public string OldValue { get; init;  }
    
    /// <summary>
    /// Новое значение
    /// </summary>
    public string NewValue { get; init; }
}