﻿using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.Interfaces;

public interface IUserService
{
    public Task<User> UpdateUserAsync(UserId userid, DateOfBirth dateOfBirth, Name name);

    public Task UpdateUserPasswordAsync(UserId userid, Password newPassword);
    
    public Task UpdateUserEmailAsync(UserId userid, EmaiL newEmail);

    public Task DeleteUserAsync(UserId userid);

    public Task<User> GetUserInfoAsync();
}