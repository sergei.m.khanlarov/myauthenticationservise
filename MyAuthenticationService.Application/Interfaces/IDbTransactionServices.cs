﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace MyAuthenticationService.Application.Interfaces;

public interface IDbTransactionServices
{
    IExecutionStrategy CreateExecutionStrategy();
    
    Task BeginTransactionAsync();
    
    Task CommitTransactionAsync();
    
    Task RollbackTransaction();
}