﻿using System.Linq.Expressions;

using MyAuthenticationService.Domain.Entities;

namespace MyAuthenticationService.Application.Interfaces;

public interface IRecordChangedService
{
    public Task<ICollection<RecordChanged>> GetRecords(Expression<Func<RecordChanged?, bool>> filter = default);
}