﻿using System.Runtime.CompilerServices;

namespace MyAuthenticationService.Application.Extensions;

public static class ObjectExtension
{
    public static string GetCallerName(this object caller, [CallerMemberName]string name = "") => $"{caller.GetType().Name}.{name}";
}