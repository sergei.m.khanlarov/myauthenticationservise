﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.GetUser;

public class GetUserQuery : IRequest<UserViewModel>
{
    /// <summary>
    /// User id
    /// </summary>
    public string UserId { get; init; }

    public GetUserQuery(string userId) => UserId = userId;
}