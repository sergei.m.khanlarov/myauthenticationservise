﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.GetUser;

public class GetUserHandler : IRequestHandler<GetUserQuery, UserViewModel>
{
    private readonly IMapper _mapper;
    private readonly IUserRepository _repository;

    public GetUserHandler(IMapper mapper, IUserRepository repository)
    {
        _mapper = mapper;
        _repository = repository;
    }

    public async Task<UserViewModel> Handle(GetUserQuery request, CancellationToken cancellationToken)
    {
        var id = UserId.Parse(request.UserId);

        var user = await _repository.GetByIdAsync(id);

        if (user is null)
            throw new NotFoundException($"User with id \"{id}\" was not found");
        
        return _mapper.Map<UserViewModel>(user);
    }
}