﻿using FluentValidation;

namespace MyAuthenticationService.Application.UsesCases.User.UpdateEmail;

public class UpdateEmailCommandValidator : AbstractValidator<UpdateEmailDto>
{
    public UpdateEmailCommandValidator()
    {
        RuleFor(user => user.NewEmail).NotEmpty().WithMessage("Email is empty");
        RuleFor(user => user.NewEmail).EmailAddress().WithMessage("Email is not actual email");
    }
}