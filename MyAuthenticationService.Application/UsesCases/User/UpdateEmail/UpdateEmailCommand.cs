﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.UpdateEmail;

public class UpdateEmailCommand : IRequest<string>
{
    /// <summary>
    /// User id
    /// </summary>
    public string UserId { get; init; }
    
    /// <summary>
    /// новый Email
    /// </summary>
    public string NewEmail { get; init; }

    public UpdateEmailCommand(string userId, string newEmail)
    {
        UserId = userId;
        NewEmail = newEmail;
    }
}