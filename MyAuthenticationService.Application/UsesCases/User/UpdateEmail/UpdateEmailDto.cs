﻿namespace MyAuthenticationService.Application.UsesCases.User.UpdateEmail;

public class UpdateEmailDto
{
    /// <summary>
    /// новый Email
    /// </summary>
    public string NewEmail { get; init; }
}