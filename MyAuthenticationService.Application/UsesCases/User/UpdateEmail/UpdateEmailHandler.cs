﻿using MediatR;

using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.UpdateEmail;

public class UpdateEmailHandler : IRequestHandler<UpdateEmailCommand, string>
{
    private readonly IUserService _userService;

    public UpdateEmailHandler(IUserService service) => _userService = service;

    public async Task<string> Handle(UpdateEmailCommand request, CancellationToken cancellationToken)
    {
        await _userService.UpdateUserEmailAsync(userid: UserId.Parse(request.UserId), 
                                              EmaiL.Parse(request.NewEmail));
        
        return $"Email has been changed successfully! new Email is {request.NewEmail}";
    }
}