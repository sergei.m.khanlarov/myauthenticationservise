﻿using System.Text.RegularExpressions;

using FluentValidation;

namespace MyAuthenticationService.Application.UsesCases.User.UpdatePassword;

public class UpdatePasswordCommandValidator : AbstractValidator<UpdatePasswordDto>
{
    public UpdatePasswordCommandValidator()
    {
        RuleFor(user => user.NewPassword).NotEmpty().WithMessage("Password is empty");
        RuleFor(user => user.NewPassword)
               .Must(password => password.Length > 5)
               .WithMessage("must contain at least 6 characters,");
        RuleFor(user => user.NewPassword)
               .Must(password => !(new Regex(@"[*&{}|+,]+")).IsMatch(password))
               .WithMessage("must not be characters from the set: * & {} | +,");
        RuleFor(user => user.NewPassword)
               .Must(password => (new Regex(@"[A-Z]+")).IsMatch(password))
               .WithMessage("must contain capital and non-capital letters,");
        RuleFor(user => user.NewPassword)
               .Must(password => (new Regex(@"[a-z]+")).IsMatch(password))
               .WithMessage("must contain capital and non-capital letters,");
        RuleFor(user => user.NewPassword)
               .Must(password => (new Regex(@"[0-9]+")).IsMatch(password))
               .WithMessage("must contain numbers");
        RuleFor(user => user.ConfirmNewPassword).NotEmpty().WithMessage("Password is empty");
        RuleFor(user => user.NewPassword).Matches(user => user.ConfirmNewPassword).WithMessage("Passwords does not match");
    }
}