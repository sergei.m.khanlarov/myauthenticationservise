﻿using MediatR;

using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.UpdatePassword;

public class UpdatePasswordHandler : IRequestHandler<UpdatePasswordCommand, string>
{
    private readonly IUserService _userService;

    public UpdatePasswordHandler(IUserService service) => _userService = service;

    public async Task<string> Handle(UpdatePasswordCommand request, CancellationToken cancellationToken)
    {
        await _userService.UpdateUserPasswordAsync(userid: UserId.Parse(request.UserId), 
                                              Password.Parse(request.NewPassword));
        
        return "Password has been changed successfully!";
    }
}