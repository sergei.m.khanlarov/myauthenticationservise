﻿namespace MyAuthenticationService.Application.UsesCases.User.UpdatePassword;

public class UpdatePasswordDto
{
    /// <summary>
    /// новый пароль
    /// </summary>
    public string NewPassword { get; init; }
    
    /// <summary>
    /// Подтверждение пароля
    /// </summary>
    public string ConfirmNewPassword { get; init; }
}