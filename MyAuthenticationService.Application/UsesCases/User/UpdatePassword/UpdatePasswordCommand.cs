﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.UpdatePassword;

public class UpdatePasswordCommand : IRequest<string>
{
    /// <summary>
    /// User id
    /// </summary>
    public string UserId { get; init; }
    
    /// <summary>
    /// новый пароль
    /// </summary>
    public string NewPassword { get; init; }
    
    /// <summary>
    /// Подтверждение пароля
    /// </summary>
    public string ConfirmNewPassword { get; init; }

    public UpdatePasswordCommand(string userId, string newPassword, string confirmNewPassword)
    {
        UserId = userId;
        NewPassword = newPassword;
        ConfirmNewPassword = confirmNewPassword;
    }
}