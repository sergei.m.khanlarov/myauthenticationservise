﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Enums;
using MyAuthenticationService.Domain.Interfaces;

namespace MyAuthenticationService.Application.UsesCases.User.GetAllUsers;

public class GetAllUsersHandler : IRequestHandler<GetAllUsersQuery, ICollection<UserViewModel>>
{
    private readonly IMapper _mapper;
    private readonly IUserRepository _repository;
    private const int _count = 4;

    public GetAllUsersHandler(IMapper mapper, IUserRepository repository)
    {
        _mapper = mapper;
        _repository = repository;
    }

    public async Task<ICollection<UserViewModel>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
    {
        var users = await _repository.GetUsers(u => 
                                                       request.Role != null ? u.Role == (Role) Enum.Parse(typeof(Role), request.Role, true) : true);

        if (!users.Any())
            throw new NotFoundException($"No users were founded");

        int maxPages = users.Count / _count + 1;
        int page = request.Page;
        
        if (page > 0)
        {
            if (page > maxPages)
                page = maxPages;
            
            var index = (page - 1) * _count;

            var count = 0;

            if (users.Count - index - _count < 0)
                count = users.Count - index;
            else
                count = _count;
            
            users = users.ToList().GetRange(index, count);
        }
        
        return _mapper.Map<ICollection<Domain.Entities.User>, ICollection<UserViewModel>>(users);
    }
}