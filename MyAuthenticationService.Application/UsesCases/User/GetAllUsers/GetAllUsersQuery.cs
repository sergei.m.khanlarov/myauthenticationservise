﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Enums;

namespace MyAuthenticationService.Application.UsesCases.User.GetAllUsers;

public class GetAllUsersQuery : IRequest<ICollection<UserViewModel>>
{
    /// <summary>
    /// Роль
    /// </summary>
    public string? Role { get; init; }
    
    /// <summary>
    /// страница
    /// </summary>
    public int Page { get; init; }

    public GetAllUsersQuery(string? role, int page)
    {
        Role = role;
        Page = page;
    }
}