﻿using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.LoginUser;

public class LoginUserResponse
{
    /// <summary>
    /// Данные о пользователе
    /// </summary>
    public UserViewModel User { get; init; }
    
    /// <summary>
    /// Токен
    /// </summary>
    public string Token { get; init; }
}