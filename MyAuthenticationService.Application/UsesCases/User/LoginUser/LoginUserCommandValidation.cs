﻿using FluentValidation;

namespace MyAuthenticationService.Application.UsesCases.User.LoginUser;

public class LoginUserCommandValidation : AbstractValidator<LoginUserCommand>
{
    public LoginUserCommandValidation()
    {
        RuleFor(command => command.Login).NotEmpty().WithMessage("Login or Password is empty");
        RuleFor(command => command.Password).NotEmpty().WithMessage("Login or Password is empty");
    }
}