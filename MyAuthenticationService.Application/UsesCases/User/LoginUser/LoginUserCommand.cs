﻿using MediatR;

namespace MyAuthenticationService.Application.UsesCases.User.LoginUser;

public class LoginUserCommand : IRequest<LoginUserResponse>
{
    /// <summary>
    /// login
    /// </summary>
    public string  Login { get; init; }
    
    /// <summary>
    /// password
    /// </summary>
    public string Password { get; init; }
}