﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Application.Token;
using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.LoginUser;

public class LoginUserHandler : IRequestHandler<LoginUserCommand, LoginUserResponse>
{
    private readonly IMapper _mapper;
    private readonly IUserRepository _repository;

    public LoginUserHandler(IMapper mapper, IUserRepository repository)
    {
        _mapper = mapper;
        _repository = repository;
    }

    public async Task<LoginUserResponse> Handle(LoginUserCommand request, CancellationToken cancellationToken)
    {
        var login = Login.Parse(request.Login);
        var password = Password.ParseForLogin(request.Password);
        var user = await _repository.GetByLoginAsync(login);

        if (user is null)
            throw new NotFoundException($"User with login \"{login}\" was not found");

        if (user.IsDeleted)
            throw new ArgumentException($"User with login {user.Login} has been deleted!");

        if (user.Password != password)
            throw new ArgumentException("login or password is incorrect");

        return new LoginUserResponse()
               {
                   User = _mapper.Map<UserViewModel>(user),
                   Token = JwtTokenGeneration.BuildTokenForUser(user)
               };
    }
}