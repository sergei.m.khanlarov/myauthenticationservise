﻿namespace MyAuthenticationService.Application.UsesCases.User.UpdateUser;

public class UpdateUserDto
{
    /// <summary>
    /// Date of dirth
    /// </summary>
    public DateOnly DateOfBirth { get; init; }

    /// <summary>
    /// first name
    /// </summary>
    public string First { get; init; }
    
    /// <summary>
    /// middle name
    /// </summary>
    public string? Middle { get; init; }
    
    /// <summary>
    /// last name
    /// </summary>
    public string Last { get; init; }
}