﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.UpdateUser;

public class UpdateUserCommand : IRequest<UserViewModel>
{
    /// <summary>
    /// User id
    /// </summary>
    public string UserId { get; init; }
    
    /// <summary>
    /// Date of dirth
    /// </summary>
    public DateOnly DateOfBirth { get; init; }

    /// <summary>
    /// first name
    /// </summary>
    public string First { get; init; }
    
    /// <summary>
    /// middle name
    /// </summary>
    public string? Middle { get; init; }
    
    /// <summary>
    /// last name
    /// </summary>
    public string Last { get; init; }

    public UpdateUserCommand(string userId, DateOnly dateOfBirth, string first,
                             string? middle,
                             string last)
    {
        UserId = userId;
        DateOfBirth = dateOfBirth;
        First = first;
        Middle = middle;
        Last = last;
    }
}