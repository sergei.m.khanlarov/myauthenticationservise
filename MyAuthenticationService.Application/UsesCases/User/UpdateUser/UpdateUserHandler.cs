﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.UpdateUser;

public class UpdateUserHandler : IRequestHandler<UpdateUserCommand, UserViewModel>
{
    private readonly IMapper _mapper;
    private readonly IUserService _userService;

    public UpdateUserHandler(IMapper mapper, IUserService service)
    {
        _mapper = mapper;
        _userService = service;
    }

    public async Task<UserViewModel> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
    {
        var user = await _userService.UpdateUserAsync(userid: UserId.Parse(request.UserId),
                                                 dateOfBirth: DateOfBirth.Parse(request.DateOfBirth),
                                                 name: Name.Parse(request.First, request.Last, request.Middle));

        return _mapper.Map<UserViewModel>(user);
    }
}