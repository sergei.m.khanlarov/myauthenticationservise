﻿using FluentValidation;

namespace MyAuthenticationService.Application.UsesCases.User.UpdateUser;

public class UpdateUserCommandValidator : AbstractValidator<UpdateUserDto>
{
    public UpdateUserCommandValidator()
    {
        RuleFor(user => user.First).NotEmpty().WithMessage("First name is empty");
        RuleFor(user => user.First).Must(c => c.All(Char.IsLetter)).WithMessage("For first name use only letters");
        RuleFor(user => user.Last).NotEmpty().WithMessage("Last name is empty");
        RuleFor(user => user.Last).Must(c => c.All(Char.IsLetter)).WithMessage("For last name use only letters");
        RuleFor(user => user.Middle)
               .Must(c => c.All(Char.IsLetter))
               .When(user => !string.IsNullOrWhiteSpace(user.Middle))
               .WithMessage("For middle name use only letters");
        RuleFor(user => user.DateOfBirth).NotEmpty().WithMessage("Date of birth is empty");
        RuleFor(user => user.DateOfBirth)
               .Must(d => DateTime.Parse(d.ToString("d")).AddYears(18).Date < DateTime.Now.Date)
               .WithMessage("You are not 18+ years old");
    }
}