﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Enums;

namespace MyAuthenticationService.Application.UsesCases.User.CreateUser;

public class CreateUserCommand : IRequest<UserViewModel>
{
    /// <summary>
    /// login
    /// </summary>
    public string  Login { get; init; }
    
    /// <summary>
    /// password
    /// </summary>
    public string Password { get; init; }
    
    /// <summary>
    /// submit password
    /// </summary>
    public string SubmitPassword { get; init; }
    
    /// <summary>
    /// Date of dirth
    /// </summary>
    public DateOnly DateOfBirth { get; init; }
    
    /// <summary>
    /// Email
    /// </summary>
    public string EMaiL { get; init; }
    
    /// <summary>
    /// first name
    /// </summary>
    public string First { get; init; }
    
    /// <summary>
    /// middle name
    /// </summary>
    public string? Middle { get; init; }
    
    /// <summary>
    /// last name
    /// </summary>
    public string Last { get; init; }
    
    /// <summary>
    /// Role
    /// </summary>
    public string Role { get; init; }
}