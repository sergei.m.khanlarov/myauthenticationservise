﻿using FluentValidation;

using MyAuthenticationService.Domain.Enums;

namespace MyAuthenticationService.Application.UsesCases.User.RegistryUser;

public class RegistryUserCommandValidator : AbstractValidator<RegistryUserCommand>
{
    public RegistryUserCommandValidator()
    {
        RuleFor(user => user.Login).NotEmpty().WithMessage("Login is empty");
        RuleFor(user => user.Login).Length(4, 30).WithMessage("Login length must be from 4 to 30 symbols");
        RuleFor(user => user.Password).NotEmpty().WithMessage("Password is empty");
        RuleFor(user => user.SubmitPassword).NotEmpty().WithMessage("Password is empty");
        RuleFor(user => user.Password).Matches(user => user.SubmitPassword).WithMessage("Passwords does not match");
        RuleFor(user => user.EMaiL).NotEmpty().WithMessage("Login is empty");
        RuleFor(user => user.EMaiL).EmailAddress().WithMessage("Email is not actual email");
        RuleFor(user => user.DateOfBirth).NotEmpty().WithMessage("Date of birth is empty");
        RuleFor(user => user.DateOfBirth)
               .Must(d => DateTime.Parse(d.ToString("d")).AddYears(18).Date < DateTime.Now.Date)
               .WithMessage("You are not 18+ years old");
        RuleFor(user => user.First).NotEmpty().WithMessage("First is empty");
        RuleFor(user => user.First).Must(c => c.All(Char.IsLetter)).WithMessage("For first name use only letters");
        RuleFor(user => user.Last).NotEmpty().WithMessage("Last is empty");
        RuleFor(user => user.Last).Must(c => c.All(Char.IsLetter)).WithMessage("For last name use only letters");
        RuleFor(user => user.Middle)
               .Must(c => c.All(Char.IsLetter))
               .When(user => !string.IsNullOrWhiteSpace(user.Middle))
               .WithMessage("For middle name use only letters");
    }
}