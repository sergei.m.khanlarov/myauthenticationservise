﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.Services;
using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Enums;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.RegistryUser;

public class RegistryUserHandler : IRequestHandler<RegistryUserCommand, UserViewModel>
{
    private readonly IMapper _mapper;
    private readonly IUserRepository _repository;

    public RegistryUserHandler(IMapper mapper, IUserRepository repository)
    {
        _mapper = mapper;
        _repository = repository;
    }

    public async Task<UserViewModel> Handle(RegistryUserCommand request, CancellationToken cancellationToken)
    {
        var user = new Domain.Entities.User(id: UserId.New(),
                                            login: Login.Parse(request.Login),
                                            password: Password.Parse(request.Password),
                                            dateOfBirth: DateOfBirth.Parse(request.DateOfBirth),
                                            email: EmaiL.Parse(request.EMaiL),
                                            name: Name.Parse(request.First, request.Last, request.Middle),
                                            role: Role.User);

        await _repository.AddAsync(user);

        SendEmailNotification.SendEmail(user.EmaiL.ToString(), user.Name.First);
        
        return _mapper.Map<UserViewModel>(user);
    }
}