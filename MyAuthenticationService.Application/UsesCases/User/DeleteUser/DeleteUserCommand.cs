﻿using MediatR;

namespace MyAuthenticationService.Application.UsesCases.User.DeleteUser;

public class DeleteUserCommand : IRequest<string>
{
    /// <summary>
    /// User id
    /// </summary>
    public string UserId { get; init; }

    public DeleteUserCommand(string userId) => UserId = userId;
}