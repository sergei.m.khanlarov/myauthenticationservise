﻿using FluentValidation;

namespace MyAuthenticationService.Application.UsesCases.User.DeleteUser;

public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
{
    public DeleteUserCommandValidator()
    {
        RuleFor(user => user.UserId).NotEmpty().WithMessage("User id is empty");
    }
}