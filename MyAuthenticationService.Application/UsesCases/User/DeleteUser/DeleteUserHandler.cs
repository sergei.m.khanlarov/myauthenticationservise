﻿using MediatR;

using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Application.UsesCases.User.DeleteUser;

public class DeleteUserHandler : IRequestHandler<DeleteUserCommand, string>
{
    private readonly IUserService _userService;

    public DeleteUserHandler(IUserService service) => _userService = service;

    public async Task<string> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
    {
        await _userService.DeleteUserAsync(userid: UserId.Parse(request.UserId));

        return $"User with id: {request.UserId} was deleted";
    }
}