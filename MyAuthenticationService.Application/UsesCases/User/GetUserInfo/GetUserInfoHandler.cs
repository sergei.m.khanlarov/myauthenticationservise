﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.GetUserInfo;

public class GetUserInfoHandler : IRequestHandler<GetUserInfoQuery, UserViewModel>
{
    private readonly IMapper _mapper;
    private readonly IUserService _userService;

    public GetUserInfoHandler(IMapper mapper, IUserService service)
    {
        _mapper = mapper;
        _userService = service;
    }

    public async Task<UserViewModel> Handle(GetUserInfoQuery request, CancellationToken cancellationToken)
    {
        var user = await _userService.GetUserInfoAsync();

        return _mapper.Map<UserViewModel>(user);
    }
}