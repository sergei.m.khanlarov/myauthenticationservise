﻿using MediatR;

using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.User.GetUserInfo;

public class GetUserInfoQuery : IRequest<UserViewModel>
{
}