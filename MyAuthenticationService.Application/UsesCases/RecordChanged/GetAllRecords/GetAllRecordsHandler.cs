﻿using AutoMapper;

using MediatR;

using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Interfaces;

namespace MyAuthenticationService.Application.UsesCases.RecordChanged.GetAllRecords;

public class GetAllRecordsHandler : IRequestHandler<GetAllRecordsQuery, ICollection<RecordChangedViewModel>>
{
    private readonly IMapper _mapper;
    private readonly IRecordChangedRepository _repository;
    private const int _count = 4;

    public GetAllRecordsHandler(IMapper mapper, IRecordChangedRepository repository)
    {
        _mapper = mapper;
        _repository = repository;
    }

    public async Task<ICollection<RecordChangedViewModel>> Handle(GetAllRecordsQuery request, CancellationToken cancellationToken)
    {
        var records = await _repository.GetRecords(r => 
                                                           (request.UserId != null ? r.UserId.Equals(request.UserId) : true) &&
                                                           (request.UpdatedBy != null ? r.ChangedByUserId.Equals(request.UpdatedBy) : true) &&
                                                           (request.Property != null ? r.Property.Equals(request.Property) : true) &&
                                                           (request.When != null ? r.ChangedAt.Date.Equals(request.When.Value.Date) : true) );
        
        int maxPages = records.Count / _count + 1;
        int page = request.Page;
        
        if (page > 0)
        {
            if (page > maxPages)
                page = maxPages;
            
            var index = (page - 1) * _count;

            var count = 0;

            if (records.Count - index - _count < 0)
                count = records.Count - index;
            else
                count = _count;
            
            records = records.ToList().GetRange(index, count);
        }

        return _mapper.Map<ICollection<Domain.Entities.RecordChanged>, ICollection<RecordChangedViewModel>>(records);
    }
}