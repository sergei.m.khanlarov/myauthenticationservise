﻿using MediatR;

using Microsoft.EntityFrameworkCore.Metadata.Internal;

using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Application.UsesCases.RecordChanged.GetAllRecords;

public class GetAllRecordsQuery : IRequest<ICollection<RecordChangedViewModel>>
{
    /// <summary>
    /// Кого правили
    /// </summary>
    public string? UserId { get; init; }
    
    /// <summary>
    /// Кто правил
    /// </summary>
    public string? UpdatedBy { get; init; }
    
    /// <summary>
    /// когда
    /// </summary>
    public DateTime? When { get; init; }
    
    /// <summary>
    /// Какую характеристику
    /// </summary>
    public string Property { get; init; }
    
    /// <summary>
    /// страница
    /// </summary>
    public int Page { get; init; }

    public GetAllRecordsQuery(int page, string? userId, string? updatedBy, 
                              DateTime? when,
                              string property)
    {
        Page = page;
        UserId = userId;
        UpdatedBy = updatedBy;
        When = when;
        Property = property;
    }
}