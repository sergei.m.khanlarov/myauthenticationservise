﻿using AutoMapper;

using MyAuthenticationService.Application.ViewModels;
using MyAuthenticationService.Domain.Entities;

namespace MyAuthenticationService.Application.AutoMappers;

public class AppMappingProfile : Profile
{
    public AppMappingProfile()
    {
        CreateMap<User, UserViewModel>();
        CreateMap<RecordChanged, RecordChangedViewModel>();
    }
}
