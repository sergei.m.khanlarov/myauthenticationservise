﻿using FluentValidation;
using FluentValidation.AspNetCore;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

using MyAuthenticationService.Application.AutoMappers;
using MyAuthenticationService.Application.Behaviours;
using MyAuthenticationService.Application.Extensions;
using MyAuthenticationService.Application.Services;
using MyAuthenticationService.Application.Token;
using MyAuthenticationService.Application.UsesCases.RecordChanged.GetAllRecords;
using MyAuthenticationService.Application.UsesCases.User.CreateUser;
using MyAuthenticationService.Application.UsesCases.User.DeleteUser;
using MyAuthenticationService.Application.UsesCases.User.GetAllUsers;
using MyAuthenticationService.Application.UsesCases.User.GetUser;
using MyAuthenticationService.Application.UsesCases.User.GetUserInfo;
using MyAuthenticationService.Application.UsesCases.User.LoginUser;
using MyAuthenticationService.Application.UsesCases.User.RegistryUser;
using MyAuthenticationService.Application.UsesCases.User.UpdateEmail;
using MyAuthenticationService.Application.UsesCases.User.UpdatePassword;
using MyAuthenticationService.Application.UsesCases.User.UpdateUser;
using MyAuthenticationService.Application.ViewModels;

using Serilog;

namespace MyAuthenticationService.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddSingleton(Log.Logger);

        services.AddMediatorPipeline<CreateUserCommand, UserViewModel>()
                .AddStep<LoggingBehaviour<CreateUserCommand, UserViewModel>>()
                .AddStep<TransactionBehaviour<CreateUserCommand, UserViewModel>>()
                .AddRequestHandler<CreateUserHandler>();
        
        services.AddMediatorPipeline<RegistryUserCommand, UserViewModel>()
                .AddStep<LoggingBehaviour<RegistryUserCommand, UserViewModel>>()
                .AddStep<TransactionBehaviour<RegistryUserCommand, UserViewModel>>()
                .AddRequestHandler<RegistryUserHandler>();

        services.AddMediatorPipeline<DeleteUserCommand, string>()
                .AddStep<LoggingBehaviour<DeleteUserCommand, string>>()
                .AddStep<TransactionBehaviour<DeleteUserCommand, string>>()
                .AddRequestHandler<DeleteUserHandler>();

        services.AddMediatorPipeline<GetAllUsersQuery, ICollection<UserViewModel>>()
                .AddStep<LoggingBehaviour<GetAllUsersQuery, ICollection<UserViewModel>>>()
                .AddRequestHandler<GetAllUsersHandler>();

        services.AddMediatorPipeline<GetAllRecordsQuery, ICollection<RecordChangedViewModel>>()
                .AddStep<LoggingBehaviour<GetAllRecordsQuery, ICollection<RecordChangedViewModel>>>()
                .AddRequestHandler<GetAllRecordsHandler>();
        
        services.AddMediatorPipeline<GetUserQuery, UserViewModel>()
                .AddStep<LoggingBehaviour<GetUserQuery, UserViewModel>>()
                .AddRequestHandler<GetUserHandler>();
        
        services.AddMediatorPipeline<GetUserInfoQuery, UserViewModel>()
                .AddStep<LoggingBehaviour<GetUserInfoQuery, UserViewModel>>()
                .AddRequestHandler<GetUserInfoHandler>();

        services.AddMediatorPipeline<LoginUserCommand, LoginUserResponse>()
                .AddStep<LoggingBehaviour<LoginUserCommand, LoginUserResponse>>()
                .AddRequestHandler<LoginUserHandler>();

        services.AddMediatorPipeline<UpdateUserCommand, UserViewModel>()
                .AddStep<LoggingBehaviour<UpdateUserCommand, UserViewModel>>()
                .AddStep<TransactionBehaviour<UpdateUserCommand, UserViewModel>>()
                .AddRequestHandler<UpdateUserHandler>();
        
        services.AddMediatorPipeline<UpdatePasswordCommand, string>()
                .AddStep<LoggingBehaviour<UpdatePasswordCommand, string>>()
                .AddStep<TransactionBehaviour<UpdatePasswordCommand, string>>()
                .AddRequestHandler<UpdatePasswordHandler>();
        
        services.AddMediatorPipeline<UpdateEmailCommand, string>()
                .AddStep<LoggingBehaviour<UpdateEmailCommand, string>>()
                .AddStep<TransactionBehaviour<UpdateEmailCommand, string>>()
                .AddRequestHandler<UpdateEmailHandler>();

        services.AddFluentValidationAutoValidation();

        services.AddScoped<AuthenticationServiceCustom>();

        services.AddHttpContextAccessor();

        services.AddValidatorsFromAssemblyContaining<CreateUserCommandValidator>();

        services.AddAutoMapper(typeof(AppMappingProfile));

        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                 {
                         options.RequireHttpsMetadata = false;
                         options.TokenValidationParameters = new TokenValidationParameters
                             {
                                     // укзывает, будет ли валидироваться издатель при валидации токена
                                     ValidateIssuer = true,
                                     // строка, представляющая издателя
                                     ValidIssuer = AuthOptions.ISSUER,
                                     // будет ли валидироваться потребитель токена
                                     ValidateAudience = true,
                                     // установка потребителя токена
                                     ValidAudience = AuthOptions.AUDIENCE,
                                     // будет ли валидироваться время существования
                                     ValidateLifetime = true,
                                     // установка ключа безопасности
                                     IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                                     // валидация ключа безопасности
                                     ValidateIssuerSigningKey = true,
                             };
                 });

        return services;
    }
}