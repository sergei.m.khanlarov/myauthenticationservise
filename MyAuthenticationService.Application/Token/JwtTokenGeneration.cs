﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

using Microsoft.IdentityModel.Tokens;

using MyAuthenticationService.Domain.Entities;

namespace MyAuthenticationService.Application.Token;

public class JwtTokenGeneration
{
    public static string BuildTokenForUser(User user)
    {
        var signingCredentials 
                = new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), 
                                         SecurityAlgorithms.HmacSha256);

        var claims = new List<Claim>
                     {
                         new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                         new Claim("id", user.Id.ToString()),
                         new Claim("login", user.Login.ToString()),
                         new Claim("name", user.Name.ToString()),
                         new Claim("role", user.Role.ToString()),
                     };

        var securityToken = new JwtSecurityToken(
                                                 issuer: AuthOptions.ISSUER,
                                                 expires: DateTime.Now.AddMinutes(AuthOptions.LIFETIME),
                                                 claims: claims,
                                                 audience: AuthOptions.AUDIENCE,
                                                 notBefore: DateTime.UtcNow,
                                                 signingCredentials: signingCredentials);

        return new JwtSecurityTokenHandler().WriteToken(securityToken);
    }
}