﻿using Microsoft.EntityFrameworkCore.Storage;

using MyAuthenticationService.Application.Extensions;
using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Infrastructure.Contexts;

using Serilog;

namespace MyAuthenticationService.Infrastructure.Services;

public class DbTransactionServices : BaseService, IDbTransactionServices
{
    private readonly ApplicationDbContext _dbContext;

    public DbTransactionServices(ApplicationDbContext dbContext, ILogger logger) : base(logger)
    {
        _dbContext = dbContext;
    }

    public IExecutionStrategy CreateExecutionStrategy()
    {
        LogInfo($"{this.GetCallerName()} started");
        
        return _dbContext.Database.CreateExecutionStrategy();
    }

    public async Task BeginTransactionAsync()
    {
        LogInfo($"{this.GetCallerName()} started");
        
        await _dbContext.Database.BeginTransactionAsync();
    }

    public async Task CommitTransactionAsync()
    {
        LogInfo($"{this.GetCallerName()} started");
        
        await _dbContext.SaveChangesAsync();
        await _dbContext.Database.CommitTransactionAsync();
    }

    public async Task RollbackTransaction()
    {
        LogInfo($"{this.GetCallerName()} started");
        
        await _dbContext.Database.RollbackTransactionAsync();
    }
}