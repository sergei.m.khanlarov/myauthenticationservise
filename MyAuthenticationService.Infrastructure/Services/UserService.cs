﻿using Microsoft.AspNetCore.Http;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Application.Extensions;
using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;

using Serilog;

namespace MyAuthenticationService.Infrastructure.Services;

public class UserService : BaseService, IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IRecordChangedRepository _recordChangedRepository;
    private readonly IHttpContextAccessor _context;
    private readonly ILogger _logger;
    
    public UserService(IUserRepository userRepository, 
                        IRecordChangedRepository recordChangedRepository, 
                        IHttpContextAccessor context,
                        ILogger logger) : base(logger)
    {
        _userRepository = userRepository;
        _recordChangedRepository = recordChangedRepository;
        _context = context;
        _logger = logger;
    }

    async Task<User> IUserService.UpdateUserAsync(UserId userid, DateOfBirth dateOfBirth, Name name)
    {
        _logger.Information($"{this.GetCallerName()} started");
        
        var user = await GetUserAsync(userid);
        
        var recordDOB = new RecordChanged(id: RecordChangedId.New(),
                                       userId: user.Id.ToString(),
                                       changedByUserId: UpdatedBy(),
                                       property: nameof(dateOfBirth),
                                       oldValue: user.DateOfBirth.ToString(),
                                       newValue: dateOfBirth.ToString());
        
        var recordN = new RecordChanged(id: RecordChangedId.New(),
                                       userId: user.Id.ToString(),
                                       changedByUserId: UpdatedBy(),
                                       property: nameof(name),
                                       oldValue: user.Name.ToString(),
                                       newValue: name.ToString());
        
        user.Update(dateOfBirth: dateOfBirth, name: name);

        await _userRepository.UpdateAsync(user);

        await _recordChangedRepository.AddAsync(recordDOB);
        await _recordChangedRepository.AddAsync(recordN);

        return user;
    }

    async Task IUserService.UpdateUserPasswordAsync(UserId userid, Password newPassword)
    {
        _logger.Information($"{this.GetCallerName()} started");
        
        var user = await GetUserAsync(userid);
        
        var record = new RecordChanged(id: RecordChangedId.New(),
                                          userId: user.Id.ToString(),
                                          changedByUserId: UpdatedBy(),
                                          property: nameof(user.Password),
                                          oldValue: user.Password.ToString(),
                                          newValue: newPassword.ToString());

        user.SetNewPassword(newPassword);

        await _userRepository.UpdateAsync(user);

        await _recordChangedRepository.AddAsync(record);
    }

    async Task IUserService.UpdateUserEmailAsync(UserId userid, EmaiL newEmail)
    {
        _logger.Information($"{this.GetCallerName()} started");
        
        var user = await GetUserAsync(userid);

        var record = new RecordChanged(id: RecordChangedId.New(),
                                       userId: user.Id.ToString(),
                                       changedByUserId: UpdatedBy(),
                                       property: nameof(user.EmaiL),
                                       oldValue: user.EmaiL.ToString(),
                                       newValue: newEmail.ToString());

        user.UpdateEmail(newEmail);

        await _userRepository.UpdateAsync(user);

        await _recordChangedRepository.AddAsync(record);
    }

    async Task IUserService.DeleteUserAsync(UserId userid)
    {
        _logger.Information($"{this.GetCallerName()} started");
        
        var user = await GetUserAsync(userid);
        
        var record = new RecordChanged(id: RecordChangedId.New(),
                                       userId: user.Id.ToString(),
                                       changedByUserId: UpdatedBy(),
                                       property: nameof(user.IsDeleted),
                                       oldValue: user.IsDeleted.ToString(),
                                       newValue: true.ToString());
        
        user.Delete();
        
        await _userRepository.UpdateAsync(user);

        await _recordChangedRepository.AddAsync(record);
    }

    async Task<User> IUserService.GetUserInfoAsync()
    {
        _logger.Information($"{this.GetCallerName()} started");
        
        var userId = _context.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "id")?.Value;

        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException("Please login");
        
        return await GetUserAsync(UserId.Parse(userId));
    }

    private string UpdatedBy() => _context.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "id")?.Value ?? "unknown";

    private async Task<User> GetUserAsync(UserId userId)
    {
        _logger.Information($"{this.GetCallerName()} started");
        
        var user = await _userRepository.GetByIdAsync(userId);

        if (user is null)
            throw new NotFoundException($"User with id \"{userId}\" was not found");
        
        return user;
    }
}