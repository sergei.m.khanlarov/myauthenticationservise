﻿using System;

using Serilog;

namespace MyAuthenticationService.Infrastructure.Services;

public class BaseService
{
    private readonly ILogger _logger;

    protected BaseService(ILogger logger)
    {
        ArgumentNullException.ThrowIfNull(logger);
        _logger = logger;
    }

    protected void LogInfo(string message) => _logger.Information(message);
    
    protected void LogError(Exception exception, string message) => _logger.Error(exception, message);
}