﻿using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using MyAuthenticationService.Domain.Base;

namespace MyAuthenticationService.Infrastructure.Extensions;

public static class LinqExtension
{
    public static IQueryable<TEntity> WhereById<TId, TEntity>(this IQueryable<TEntity> entities, TId? id)
            where TEntity : Entity<TId>
            where TId : BaseId
    {
        return id is null ? entities : entities.Where(EqualsPredicate<TId, TEntity>(id));
    }
    
    public static IQueryable<TEntity> WhereByIds<TId, TEntity>(this IQueryable<TEntity> entities, IEnumerable<TId>? ids)
            where TEntity : Entity<TId>
            where TId : BaseId
    {
        return ids is null ? entities : entities.Where(e => ids.Contains(e.Id));
    }

    public static IQueryable<TEntity> WhereByFilter<TId, TEntity>(this IQueryable<TEntity> entities, Expression<Func<TEntity, bool>>? filter)
            where TEntity : Entity<TId>
            where TId : BaseId
    {
        return filter is null ? entities : entities.Where(filter);
    }

    public static IQueryable<TEntity> IncludeProperties<TId, TEntity>(this IQueryable<TEntity> entities, IEnumerable<string>? properties)
            where TEntity : Entity<TId>
            where TId : BaseId
    {
        return properties is null
                       ? entities
                       : properties.Aggregate(entities, (current, nameProperty) => current.Include(nameProperty));
    }

    public static IQueryable<TEntity> OrderBy<TId, TEntity>(this IQueryable<TEntity> entities,
                                                            Expression<Func<TEntity, object>>? orderBy,
                                                            Expression<Func<TEntity, object>>? orderByDescending)
            where TEntity : Entity<TId>
            where TId : BaseId
    {
        return (orderBy, orderByDescending) switch
               {
                   ({ }, null) => entities.OrderBy(orderBy),
                   (null, { }) => entities.OrderByDescending(orderByDescending),
                   _           => entities
               };
    }
    
    private static Expression<Func<TEntity, bool>> EqualsPredicate<TId, TEntity>(TId id) where TEntity : Entity<TId> where TId : BaseId
    {
        Expression<Func<TEntity, TId>> selector = x => x.Id;
        Expression<Func<TId>> closure = () => id;

        return Expression.Lambda<Func<TEntity, bool>>(Expression.Equal(selector.Body, closure.Body), selector.Parameters);
    }
}