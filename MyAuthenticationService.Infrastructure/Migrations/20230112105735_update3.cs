﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyAuthenticationService.Infrastructure.Migrations
{
    public partial class update3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EMaiL",
                schema: "Users",
                table: "Users",
                newName: "EmaiL");

            migrationBuilder.RenameIndex(
                name: "IX_Users_EMaiL",
                schema: "Users",
                table: "Users",
                newName: "IX_Users_EmaiL");

            migrationBuilder.CreateTable(
                name: "RecordChanges",
                schema: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ChangedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    ChangedByUserId = table.Column<string>(type: "text", nullable: false),
                    Property = table.Column<string>(type: "text", nullable: false),
                    OldValue = table.Column<string>(type: "text", nullable: false),
                    NewValue = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordChanges", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RecordChanges",
                schema: "Users");

            migrationBuilder.RenameColumn(
                name: "EmaiL",
                schema: "Users",
                table: "Users",
                newName: "EMaiL");

            migrationBuilder.RenameIndex(
                name: "IX_Users_EmaiL",
                schema: "Users",
                table: "Users",
                newName: "IX_Users_EMaiL");
        }
    }
}
