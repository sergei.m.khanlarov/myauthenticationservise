﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyAuthenticationService.Infrastructure.Migrations
{
    public partial class userInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Users");

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Login = table.Column<string>(type: "text", nullable: false),
                    Password = table.Column<string>(type: "text", nullable: false),
                    DateOfBirth = table.Column<string>(type: "text", nullable: false),
                    EMaiL = table.Column<string>(type: "text", nullable: false),
                    Name_First = table.Column<string>(type: "text", nullable: false),
                    Name_Middle = table.Column<string>(type: "text", nullable: true),
                    Name_Last = table.Column<string>(type: "text", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_EMaiL",
                schema: "Users",
                table: "Users",
                column: "EMaiL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Login",
                schema: "Users",
                table: "Users",
                column: "Login",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users",
                schema: "Users");
        }
    }
}
