﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.Enums;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Infrastructure.Configurations;
public class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.Property(e => e.Id).ValueGeneratedNever();
        
        builder.Property(e => e.Id)
               .HasConversion(id => id.ToString(),
                              value => UserId.Parse(value));
        
        builder.HasIndex(e => e.Login).IsUnique();
        
        builder.Property(e => e.Login)
               .HasConversion(login => login.ToString(),
                              value => Login.Parse(value));
        
        builder.HasIndex(e => e.EmaiL);
        
        builder.Property(e => e.EmaiL)
               .HasConversion(emaiL => emaiL.ToString(),
                              value => EmaiL.Parse(value));
        
        builder.Property(e => e.Password)
               .HasConversion(password => password.ToString(),
                              value => Password.ParseNoHashing(value));
        
        builder.Property(e => e.DateOfBirth)
               .HasConversion(dateOfBirth => dateOfBirth.ToString(),
                              value => DateOfBirth.Parse(DateOnly.Parse(value)));
        
        builder.Property(e => e.Role)
               .HasConversion(role => role.ToString(),
                              value => (Role) Enum.Parse(typeof(Role), value, true));
        
        builder.OwnsOne(e => e.Name,
                        builder =>
                        {
                            builder.Property(t => t.First);
                            builder.Property(t => t.Middle);
                            builder.Property(t => t.Last);
                        });

        builder.Property(e => e.IsDeleted);
    }
}
