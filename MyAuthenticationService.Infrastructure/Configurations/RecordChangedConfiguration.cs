﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.ValueObjects;

namespace MyAuthenticationService.Infrastructure.Configurations;

public class RecordChangedConfiguration : IEntityTypeConfiguration<RecordChanged>
{
    public void Configure(EntityTypeBuilder<RecordChanged> builder)
    {
        builder.Property(e => e.Id).ValueGeneratedNever();
        
        builder.Property(e => e.Id)
               .HasConversion(id => id.ToString(),
                              value => RecordChangedId.Parse(value));
        
        builder.Property(e => e.ChangedAt);
        
        builder.Property(e => e.NewValue);
        
        builder.Property(e => e.OldValue);
        
        builder.Property(e => e.Property);
        
        builder.Property(e => e.ChangedByUserId);
        
        builder.Property(e => e.UserId);
    }
}