﻿using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;
using MyAuthenticationService.Infrastructure.Contexts;

using Serilog;

namespace MyAuthenticationService.Infrastructure.Repositories;

public class RecordChangedRepository : BaseRepository, IRecordChangedRepository
{
    public RecordChangedRepository(ApplicationDbContext context, ILogger logger) : base(context, logger)
    {
    }

    public async Task<RecordChanged> GetByIdAsync(RecordChangedId id) 
        => await Context.RecordChanges.FirstOrDefaultAsync(r => r.Id == id);

    public async Task<ICollection<RecordChanged?>> GetAllAsync() => await Context.RecordChanges.ToListAsync();
    
    public IQueryable<RecordChanged> GetAll() => Context.RecordChanges;
    
    public async Task<ICollection<RecordChanged?>> GetRecords(Expression<Func<RecordChanged?, bool>> filter = default)
    {
        var records = GetAll();

        var result = await records.Where(filter).ToListAsync();

        if (!result.Any())
            throw new NotFoundException($"No records were founded");

        return result;
    }

    public async Task AddAsync(RecordChanged? record) => await Context.RecordChanges.AddAsync(record);
}