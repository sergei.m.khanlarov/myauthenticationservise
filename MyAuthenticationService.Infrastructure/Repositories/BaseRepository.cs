﻿using MyAuthenticationService.Infrastructure.Contexts;

using Serilog;

namespace MyAuthenticationService.Infrastructure.Repositories;

public abstract class BaseRepository
{
    private readonly ILogger _logger;
    protected ApplicationDbContext Context { get; }

    protected BaseRepository(ApplicationDbContext context, ILogger logger)
    {
        ArgumentNullException.ThrowIfNull(logger);
        ArgumentNullException.ThrowIfNull(context);
        _logger = logger;
        Context = context;
    }

    protected void LogInfo(string message) => _logger.Information(message);
}