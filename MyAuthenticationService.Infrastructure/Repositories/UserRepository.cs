﻿using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Domain.Entities;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Domain.ValueObjects;
using MyAuthenticationService.Infrastructure.Contexts;

using Serilog;

namespace MyAuthenticationService.Infrastructure.Repositories;

public class UserRepository : BaseRepository, IUserRepository
{ 
    public UserRepository(ApplicationDbContext context, ILogger logger) : base(context, logger)
    {
    }

    public async Task<User> GetByIdAsync(UserId id) => await Context.Users.FirstOrDefaultAsync(u => u.Id == id);

    public async Task<ICollection<User?>> GetAllAsync() => await Context.Users.ToListAsync();

    public IQueryable<User?> GetAll() => Context.Users;
    
    public async Task<ICollection<User?>> GetUsers(Expression<Func<User?, bool>> filter = default)
    {
        var users = GetAll();

        var result = await users.Where(filter).ToListAsync();

        if (!result.Any())
            throw new NotFoundException($"No users were founded");

        return result;
    }

    public async Task UpdateAsync(User? user) => Context.Users.Update(user);

    public async Task AddAsync(User? user) => await Context.Users.AddAsync(user);

    public async Task<User?> GetByLoginAsync(Login login) => await Context.Users.FirstOrDefaultAsync(u => u.Login.Equals(login));
}