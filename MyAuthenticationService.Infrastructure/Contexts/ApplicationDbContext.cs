using System.Reflection;

using Microsoft.EntityFrameworkCore;

using MyAuthenticationService.Domain.Entities;

namespace MyAuthenticationService.Infrastructure.Contexts;

public sealed class ApplicationDbContext : DbContext
{
    public DbSet<User?> Users { get; set; }
    
    public DbSet<RecordChanged?> RecordChanges { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.HasDefaultSchema("Users");
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}