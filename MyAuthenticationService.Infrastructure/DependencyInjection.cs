﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MyAuthenticationService.Application.Interfaces;
using MyAuthenticationService.Domain.Interfaces;
using MyAuthenticationService.Infrastructure.Contexts;
using MyAuthenticationService.Infrastructure.Repositories;
using MyAuthenticationService.Infrastructure.Services;

namespace MyAuthenticationService.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IUserRepository, UserRepository>()
                .AddScoped<IRecordChangedRepository, RecordChangedRepository>();
        
        services.AddDbContext<DbContext, ApplicationDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

        services.AddScoped<IDbTransactionServices, DbTransactionServices>();
        services.AddScoped<IUserService, UserService>();
        
        services.AddHttpContextAccessor();
        
        return services;
    }
}