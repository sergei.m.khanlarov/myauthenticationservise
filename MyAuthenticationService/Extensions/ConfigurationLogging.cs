﻿using System.Text;

using ILogger = Serilog.ILogger;

namespace MyAuthenticationService.Extensions;

public static class ConfigurationLogging
{
    private const int indentSize = 2;

    public  static void LogConfigInfo(this IHost host, ILogger logger) => logger.Information($"App configuration: \n{GetSectionContent(host.Services.GetRequiredService<IConfiguration>())}");
    
    private static string GetSectionContent(IConfiguration configSection, int indent = 0)
    {
        var strb = new StringBuilder();
        var i = new string(' ', indentSize * indent);
        foreach (var section in configSection.GetChildren())
        {
            strb.Append($"{i}\"" + section.Key + "\":");

            if (section.Value == null)
            {
                string subSectionContent = GetSectionContent(section,  indent+2);
                strb.Append("{\n" + subSectionContent);
                strb.Append($"{i}");
                strb.Append("},\n");
            }
            else
            {
                strb.Append("\"" + section.Value + "\",\n");
            }
        }

        return strb.ToString();
    }
}