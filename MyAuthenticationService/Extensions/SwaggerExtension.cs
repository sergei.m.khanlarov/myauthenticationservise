﻿using System.Reflection;

using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.Filters;

namespace MyAuthenticationService.Extensions;

public static class SwaggerExtension
{
    public static void AddCustomSwaggerOptions(this IServiceCollection services)
    {
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            
        services.AddSwaggerGen(options => {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "My Authentication Api",
                Description = "Authentication service"
            });
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = "Standard Authorization header using the Bearer scheme (\"bearer {token}\")",
                In = ParameterLocation.Header,
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey
            });
            options.IncludeXmlComments(xmlPath);
            options.UseDateOnlyTimeOnlyStringConverters();
            options.OperationFilter<SecurityRequirementsOperationFilter>("Bearer");
        });
    }

    public static void UseCustomSwaggerOptions(this IApplicationBuilder app)
    {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("swagger/v1/swagger.json", "Authentication Service Test");
            c.RoutePrefix = string.Empty;
        });
    }
}