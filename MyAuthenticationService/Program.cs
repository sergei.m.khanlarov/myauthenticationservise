using MyAuthenticationService.Application;
using MyAuthenticationService.Extensions;
using MyAuthenticationService.Infrastructure;

using Serilog;

try
{
    var builder = WebApplication.CreateBuilder(args);
    
    Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Configuration).CreateLogger();

    Log.Information("Starting up the App...");
    
    builder.Host.UseSerilog();

    builder.Services.AddInfrastructure(builder.Configuration);
    builder.Services.AddApplication();
    builder.Services.AddControllers();
    builder.Services.AddCustomSwaggerOptions();
    builder.Services.AddDateOnlyTimeOnlyStringConverters();

    var app = builder.Build();

    app.UseCustomSwaggerOptions();
    app.UseHttpsRedirection();
    app.UseCors();
    app.UseRouting();
    app.UseAuthentication();
    app.UseAuthorization();
    app.MapControllers();
    
    app.UseSerilogRequestLogging();
    app.LogConfigInfo(Log.Logger);

    app.UseEndpoints(eB => eB.MapControllers());

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}