﻿using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Application.Services;
using MyAuthenticationService.Application.UsesCases.User.CreateUser;
using MyAuthenticationService.Application.UsesCases.User.DeleteUser;
using MyAuthenticationService.Application.UsesCases.User.GetAllUsers;
using MyAuthenticationService.Application.UsesCases.User.GetUser;
using MyAuthenticationService.Application.UsesCases.User.GetUserInfo;
using MyAuthenticationService.Application.UsesCases.User.LoginUser;
using MyAuthenticationService.Application.UsesCases.User.RegistryUser;
using MyAuthenticationService.Application.UsesCases.User.UpdateEmail;
using MyAuthenticationService.Application.UsesCases.User.UpdatePassword;
using MyAuthenticationService.Application.UsesCases.User.UpdateUser;
using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Controllers;

/// <summary>
/// Работа с данными пользователя
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class UsersController : Controller
{
    private IMediator Mediator => HttpContext.RequestServices.GetRequiredService<IMediator>();

    private readonly AuthenticationServiceCustom _authenticationService;

    public UsersController(AuthenticationServiceCustom authenticationService) { _authenticationService = authenticationService; }

    /// <summary>
    /// Создает пользователя (роль указывается при создании)
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] CreateUserCommand command)
    {
        try
        {
            return Ok(await Mediator.Send(command));
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Создает пользователя с ролью пользователь
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost]
    [Route("registry")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Registry([FromBody] RegistryUserCommand command)
    {
        try
        {
            return Ok(await Mediator.Send(command));
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Произвести авторизацию пользователя
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost]
    [Route("login")]
    [ProducesResponseType(typeof(LoginUserResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Login([FromBody] LoginUserCommand command)
    {
        try
        {
            return Ok(await Mediator.Send(command));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
    
    /// <summary>
    /// Получить всех пользователей
    /// </summary>
    /// <param name="role">роль</param>
    /// <param name="page">страницы</param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpGet]
    [Route("")]
    [ProducesResponseType(typeof(ICollection<UserViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAll([FromQuery]string? role, [FromQuery]int page)
    {
        try
        {
            return Ok(await Mediator.Send(new GetAllUsersQuery(role, page)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Получить данные пользователя пользователя
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [HttpGet]
    [Authorize]
    [Route("{userId}")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get([FromRoute] string userId)
    {
        try
        {
            if (!_authenticationService.IsAllowed(userId))
                return Forbid();

            return Ok(await Mediator.Send(new GetUserQuery(userId)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
    
    /// <summary>
    /// Получить данные пользователя пользователя
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [HttpGet]
    [Authorize]
    [Route("info")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetInfo()
    {
        try
        {
            return Ok(await Mediator.Send(new GetUserInfoQuery()));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Обновить данные пользователя
    /// </summary>
    /// <param name="body"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPut]
    [Route("{userId}")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update([FromBody] UpdateUserDto body, [FromRoute] string userId)
    {
        try
        {
            if (!_authenticationService.IsAllowed(userId))
                return Forbid();

            return Ok(await Mediator.Send(new UpdateUserCommand(userId,
                                                                body.DateOfBirth,
                                                                body.First,
                                                                body.Middle,
                                                                body.Last)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Обновить пароль
    /// </summary>
    /// <param name="body"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPut]
    [Route("{userId}/password")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdatePassword([FromBody] UpdatePasswordDto body, [FromRoute] string userId)
    {
        try
        {
            if (!_authenticationService.IsAllowed(userId))
                return Forbid();

            return Ok(await Mediator.Send(new UpdatePasswordCommand(userId: userId,
                                                                    newPassword: body.NewPassword,
                                                                    confirmNewPassword: body.ConfirmNewPassword)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Обновить Email
    /// </summary>
    /// <param name="body"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPut]
    [Route("{userId}/email")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateEmail([FromBody] UpdateEmailDto body, [FromRoute] string userId)
    {
        try
        {
            if (!_authenticationService.IsAllowed(userId))
                return Forbid();

            return Ok(await Mediator.Send(new UpdateEmailCommand(userId: userId,
                                                                 newEmail: body.NewEmail)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Удалить пользователя
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Route("{userId}")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete([FromRoute] string userId)
    {
        try
        {
            return Ok(await Mediator.Send(new DeleteUserCommand(userId)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}