﻿using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using MyAuthenticationService.Application.CustomExceptions;
using MyAuthenticationService.Application.UsesCases.RecordChanged.GetAllRecords;
using MyAuthenticationService.Application.ViewModels;

namespace MyAuthenticationService.Controllers;

/// <summary>
/// Работа с данными пользователя
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class RecordChangesController : Controller
{
    private IMediator Mediator => HttpContext.RequestServices.GetRequiredService<IMediator>();
    
    /// <summary>
    /// Получить записи об изменениях
    /// </summary>
    /// <param name="page"></param>
    /// <param name="userId"></param>
    /// <param name="updatedBy"></param>
    /// <param name="when"></param>
    /// <param name="property"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpGet]
    [ProducesResponseType(typeof(ICollection<RecordChangedViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAll([FromQuery]int page, [FromQuery]string? userId, [FromQuery]string? updatedBy, 
                                            [FromQuery]DateTime? when,
                                            [FromQuery]string? property)
    {
        try
        {
            return Ok(await Mediator.Send(new GetAllRecordsQuery(page, userId, updatedBy, when, property)));
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}